// SECTION - JS Synchronous cs Asynchronous
// JavaScript is by default sychronous, meaning that only one statement will be executed at a time.
// This can be proven when a statement has an error, JavaScript will not proceed with the next statement
/*console.log("Hello World");
// conosle.log("Hello Again");
console.log("Hello Again");
console.log("Goodbye");*/

// When statements take time to process, this slows down our code.
// An example of this is when loops are used on a large amount of information or when fetching data from databases
// We might not notice it due to the fast processing power of our devices
// This is the reason some websites don't instantly load and we only see a white screen at times while the application is still waiting for all the code to be executed
/*for (let i = 1; i <= 1500; i++) {
	console.log(i);
};
// this will only be displayed after the loop has finished
console.log("It's me again");*/



// SECTION - ASYNCHRONOUS

// FETCH
// Fetch API - allows us to asynchronously request for a resource (data);
// Asynchronous - allows executing of codes simultaneously prioritizing on the less resource-consuming codes while  the more complex and more resouce-consuming codes run at the back
// A "Promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value
/*
	SYNTAX:
		fetch(URL);
*/
// console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// retrieves all posts following the REST API (retrieve, /posts, GET)
fetch("https://jsonplaceholder.typicode.com/posts")
// by using the ".then" method, we can now check the status of the promise
// "fetch" method will return a "promise" that resolves to a "response" object
// the ".then" method captures the "response" object and returns another "promise" which will eventually be resolved or rejected.
// .then(response => console.log(response));

// we use the "json" method from the response object to convert the data retrieved into JSON format to be used in our application
.then(response => response.json())
// using multiple "then" methods will result into "promise chain"
// then we can now access it and log in the console our desired output.
.then( json => console.log(json));


console.log("Hello");
console.log("Hello World");
console.log("Hello Again");



// ASYNC-AWAIT
// "async" and "await" keywords are other approach that can be used to perform asychronous javascript
// used in functions to indicate which portions of the code should be waited for

// creates an asynchronous function
async function fetchData() {
	// waits for the fetch method to complete then stores it inside the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	// Result of returned fetch from the "result" variable
	console.log(result);
	// the returned value is an object
	console.log(typeof result);
	// we cannot access the content of the response object by directly accessing its body property
	console.log(result.body);

	// converts the data from the response object inside the result variable as JSON
	let json = await result.json();
	// prints out the content of the response object
	console.log(json);
};

fetchData();
// this will be executed first
console.log("Hello");


// SECTION - creating a post
/*
	SYNTAX:
		fetch(URL, options)
*/
// create a new post following REST IP (create, /posts, POST)
fetch("https://jsonplaceholder.typicode.com/posts", {
	// setting request method into "POST"
	method: "POST",
	// setting request headers to be sent to the backend
	// specified that the request headers will be sending JSON structure for its content
	headers: {
		"Content-Type": "application/json"
	},
	// set content/body data of the "request" object to be sent to the backend
	// JSON.stringify converts the object into stringified JSON
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

/*
	with this link, https://jsonplaceholder.typicode.com/posts/1,
	perform PUT method with the following request context
		header: content type = application/json
		body: stringified version of the object with the properties:
			id: 1,
			title: Updated Post
			body: Hello again
			userId: 1

	send the output in our google chat
*/

// SECTION - updating a post (PUT)
// updates a specific post following the RES API (update, /posts/:id, PUT)
	fetch( "https://jsonplaceholder.typicode.com/posts/1", {
		method: "PUT",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			id: 1
			title:"Updated Post",
			body: "Hello Again",
			userId: 1
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));


// SECTION- update a post (PATCH)
/*
	the difference between PUT and PATCH is the number of properties being updated

	PATCH is used to update a single property while maintaining the unupdated properties; used to update a part of the document

	PUT is used when all of the properties need to be updated, or the whole document itself; used to set an entity's information completely
*/
//  updates a specific post following the RES API (update, /posts/:id, PATCH)
	fetch( "https://jsonplaceholder.typicode.com/posts/1", {
		method: "PATCH",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			title:"Corrected Post"
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));

// SECTION - deleting of a resource
	fetch( "https://jsonplaceholder.typicode.com/posts/1", {
		method: "DELETE"
	} )


// SECTION - Filter posts
// the data can be filtered by sending the userId along with the URL
// information sent via the url can be done by adding the question mark symbol (?)
/*
	Syntax
		"<url>?parameterName=value" - for single parameter search
		"<url>?parameterNameA=valueA&&parameterNameB=valueB" - for multiple parameter search
*/
	fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
	.then(response => response.json())
	.then(json => console.log(json));

// Retrieving comments for a specific post/ accessing nested/embedded comments
	fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
	.then(response => response.json())
	.then(json => console.log(json));