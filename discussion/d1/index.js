// console.log("Hello World");
// console.log("Goodbye");
// for (let i= 1; i <= 1500; i++) {
// 	console.log(i);
// };

// console.log("It's me again");

// fetch("https://jsonplaceholder.typicode.com/posts")
// .then(response => response.json())


console.log("Hello");
console.log("Hello World");
console.log("Hello Again");

async function fetchData() {
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);

	console.log("Hello");
}
fetchData();

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})

.then(response => response.json())

.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated Post",
		body: "Hello Again",
		userId: 1
	})
})

.then(response => response.json())

.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post"
	})
})

.then(response => response.json())

.then(json => console.log(json));


fetch("https://jsonplaceholder.typicode.com/posts?userId=1")

.then(response => response.json())

.then(json => console.log(json));


fetch("https://jsonplaceholder.typicode.com/posts/1/comments")

.then(response => response.json())

.then(json => console.log(json));

